from selenium import webdriver
import time

# Prueba de voto con cambio de opción seleccionada
driver = webdriver.Chrome(executable_path='qaback/test_suite/drivers/chromedriver.exe')
driver.maximize_window()

driver.get("http://127.0.0.1:8000/polls/1")
driver.find_element_by_id('choice3').click()
driver.find_element_by_id('choice2').click()

# Validar que el cambio de opción seleccionada se realizó
assert (driver.find_element_by_id('choice3').is_selected()) == False
assert (driver.find_element_by_id('choice2').is_selected()) == True

# Enviar voto
driver.find_element_by_id('choice3').submit()


driver.close()
driver.quit()