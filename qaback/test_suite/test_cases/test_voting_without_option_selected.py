from selenium import webdriver
import time

# Prueba de voto sin seleccionar opción
driver = webdriver.Chrome(executable_path='qaback/test_suite/drivers/chromedriver.exe')
driver.maximize_window()

driver.get("http://127.0.0.1:8000/polls/1")
driver.find_element_by_id('choice3').submit()

# Validar que se muestre mensaje indicando obligatoriedad
assert "You didn't select a choice." in driver.page_source

driver.close()
driver.quit()